import logo from './logo.svg';
import './App.css';
import './fonts/font.css'
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Main from "./components/Main/Main";
import {API} from "./Config";
import React, {useEffect, useState} from 'react';
import Product from "./components/Main/Product";


function App() {

    const [filteredFilter, setFilteredFilter] = useState();
    const [filteredSearch, setFilteredSearch] = useState();
    let [product, setProduct] = useState();
    const [productItem, setProductItem] = useState();
    const [allSelected, setAllSelected] = useState(false)
    const [allDeselected, setAllDeselected] = useState(false)
    const [clearClicked, setClearClicked] = useState(false)
    const [productClicked, setProductClicked] = useState()
    const [clicked, setClicked] = useState(false)
    const [searchInput, setSearchInput] = useState();
    const [isLoading , setIsLoading ] = useState(true);
    const [selected, setSelected] = useState(0);
    let [minPrice, setMinPrice] = useState(0);
    let [maxPrice, setMaxPrice] = useState(0);
    const [filterObject, setFilterObject] =useState({
        sort: 'New',
        search: '',
        slideMin: minPrice,
        slideMax: maxPrice
    })
    let min;
    let max;
    // console.log(searchInput , 'searchInput')
    
    useEffect( () => {

        call(`/products`).then(productList => {

           //  max= Math.ceil(Math.max.apply(Math, productList.map((item) => { return item.price; })))
           // min = Math.floor(Math.min.apply(Math, productList.map((item) => { return item.price; })))
           //  setMinPrice(min)
           //  setMaxPrice(max)
           //
           //  setFilterObject({
           //      ...filterObject,
           //      slideMin: Math.floor(Math.min.apply(Math, productList.map((item) => { return item.price; }))),
           //      slideMax: Math.ceil(Math.max.apply(Math, productList.map((item) => { return item.price; })))
           //  })
            setMaxAndMinPrice(productList)
            // setFilteredFilter(productList)
            setProduct(productList)
            setProductItem(productList)
            setIsLoading(false)


        });
    }, [] );

    const call = async (url) => {
        const request = await fetch(API + url);
        return await request.json();
    };

    const changeSort = (e) => {
        setFilterObject({
            ...filterObject,
            sort: e
        })
        setIsLoading(true)
    }
    const changeSearch = async () => {

        // setFilterObject({
        //     ...filterObject,
        //     slideMin: min,
        //     slideMax: max
        // })
        setFilterObject({
            ...filterObject,
            search: searchInput
        })
        // changeSlider(filterObject);
        setIsLoading(true)
    }
    const changeSlideMin = (e) => {
        setFilterObject({
            ...filterObject,
            slideMin: e
        })
        setIsLoading(true)
    }
    const changeSlideMax = async (e) => {
        setFilterObject({
            ...filterObject,
            slideMax: e
        })
        setIsLoading(true)
    }

    useEffect(() => {
        oneFuncInsteadOfThree(filterObject);
    } , [filterObject])

  const setMaxAndMinPrice = (productList) => {
      max= Math.ceil(Math.max.apply(Math, productList.map((item) => { return item.price; })))
      min = Math.floor(Math.min.apply(Math, productList.map((item) => { return item.price; })))
      setMinPrice(min)
      setMaxPrice(max)

      setFilterObject({
          ...filterObject,
          slideMin: Math.floor(Math.min.apply(Math, productList.map((item) => { return item.price; }))),
          slideMax: Math.ceil(Math.max.apply(Math, productList.map((item) => { return item.price; })))
      })
  }
    const oneFuncInsteadOfThree = (e) =>{
        product = productItem;
        if(product && product.length){
            //sort
            if(e.sort == "priceDesc"){
                product.sort((a, b) => (a.price < b.price) ? 1 : -1)
            }
            else if(e.sort == "priceAsc"){
                product.sort((a, b) => (a.price > b.price) ? 1 : -1)
            }
            else if(e.sort == "AtoZ"){
                product.sort((a, b) => (a.title > b.title) ? 1 : -1)
            }
            else if(e.sort == "ZtoA"){
                product.sort((a, b) => (a.title < b.title) ? 1 : -1)
            }
            else if(e.sort == "New"){
                product.sort((a, b) => (a.id > b.id) ? 1 : -1)
            }


            // filter && search
            product = product.filter(product => {
                return (product.price >= e.slideMin)&&
                (product.price <= e.slideMax)&&
                (product.title.toUpperCase().includes(e.search.toUpperCase()))
            });


            setProduct(product);

            // setMaxAndMinPrice(product)
            // console.log("loop")
        }
        console.log(maxPrice, minPrice, "maxmin")
        setIsLoading(false)
    }


    


    return (
    <div className={"App"}>
        <Aside min={minPrice} max={maxPrice} slideMin={filterObject.slideMin} minPrice={minPrice} maxPrice={maxPrice} setSlideMin={changeSlideMin}  slideMax={filterObject.slideMax} setSlideMax={changeSlideMax}/>
     <div className={"catalog"}>
         <Header setBTNVal={changeSearch} setSearchInput={setSearchInput} filtered={filteredFilter} setFiltered={setFilteredFilter} filteredSearch={filteredSearch} setFilteredSearch={setFilteredSearch}  setSort={changeSort}  getSort={filterObject.sort} setProduct={setProduct} product={product} selected={selected} setAllSelected={setAllSelected}  allSelected={allSelected} setAllDeselected={setAllDeselected} allDeselected={allDeselected} clearClicked={clearClicked} setClearClicked={setClearClicked}/>
         <Main isLoading={isLoading} clicked={clicked} setClicked={setClicked} productClicked={productClicked} setProductClicked={setProductClicked} setProduct={setProduct} product={product} selected={selected} setSelected={setSelected} allSelected={allSelected} allDeselected={allDeselected}  setAllDeselected={setAllDeselected} clearClicked={clearClicked} setClearClicked={setClearClicked}/>
     </div>

    </div>
  );
}

export default App;
