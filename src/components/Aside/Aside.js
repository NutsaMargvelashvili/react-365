import Nav from "./Nav/Nav"
import "./Aside.css"
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons'
import Slider from "./Slider";
import Button from "../Header/Button";


const Aside = ({slideMin, setSlideMin, slideMax, setSlideMax, min, max, minPrice, maxPrice}) => {



    const [getNiche, setNiche] = useState(false);
    const dropdownVisible = () => {
        setNiche(!getNiche);
    }
    const [getCategory, setCategory] = useState(false);
    const dropdownVisible2 = () => {
        setCategory(!getCategory);
    }
    const [getCountryFrom, setCountryFrom] = useState(false);
    const showCountryFrom = () => {
        setCountryFrom(!getCountryFrom);
    }
    const [getCountryTo, setCountryTo] = useState(false);
    const showCountryTo = () => {
        setCountryTo(!getCountryTo);
    }
    const [getSupplier, setSupplier] = useState(false);
    const showSupplier = () => {
        setSupplier(!getSupplier);
    }
    return (
        <div className={"aside_wrapper"}>
            <aside className={"aside"}>

                <Nav />
                <div className={"catalog__filter"}>
                    <div className="filter__niche">
                        <ul  value={getNiche} onClick={dropdownVisible} className="choose--niche" name="choose--niche">
                            <li>Choose Niche
                                <FontAwesomeIcon className={"chevronDown" +((getNiche? "": " chevronDownVisible"))} icon={faChevronDown} />
                                <FontAwesomeIcon className={"chevronUp" +((getNiche? " chevronUpVisible": ""))} icon={faChevronUp} />
                            </li>

                               <ul className={"niche__dropdown" + ((getNiche)? " niche__dropdown--visible": "")}>
                                   <li className={"selected"} value="bestSellers"> ...</li>
                                   <li value="bestSellers"> Best Sellers</li>
                                   <li value="beauty"> Beauty</li>
                                   <li value="electronics"> Electronics</li>
                                   <li value="fashion"> Fashion</li>
                                   <li value="fragrances"> Fragrances</li>
                                   <li value="fragrances"> Health</li>
                                   <li value="fragrances"> Home $ Design</li>
                                   <li value="fragrances"> Innovative & Outlet</li>
                                   <li value="fragrances"> Jewelry</li>
                                   <li value="fragrances"> Pets</li>
                                   <li value="fragrances"> Sex Toys</li>
                                   <li value="fragrances"> Toys</li>
                               </ul>

                        </ul>
                    </div>
                    <div className="filter__category">
                        <ul value={getCategory} onClick={dropdownVisible2} className="choose--category" name="choose--category">
                            <li >
                               <p>Choose Category
                                <FontAwesomeIcon className={"chevronDown--category" +((getCategory? "": " chevronDownVisible"))} icon={faChevronDown} />
                                <FontAwesomeIcon className={"chevronUp--category" +((getCategory? " chevronUpVisible": ""))} icon={faChevronUp}/>
                               </p>
                                <ul className={"category__dropdown" + ((getCategory)? " category__dropdown--visible": "")}>
                                    <li value="val" className={"selected"}> ...</li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div className={"shipping"}>
                        <ul className={"country--from--header"}>
                            <li className={"country--from--header-title"}  value={getCountryFrom} onClick={showCountryFrom}>Ship From
                                <FontAwesomeIcon className={"chevronVisible" + ((getCountryFrom? " chevronInvisible" : ""))} icon={faChevronDown} />
                                <FontAwesomeIcon className={"chevronVisible" + ((getCountryFrom? "" : " chevronInvisible"))} icon={faChevronUp}/>
                            </li>
                             <ul className={"country--from--list" + ((getCountryFrom)? " country--from--list--visible": "")} >
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇦🇺 <span>Australia</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇨🇳 <span>China</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇨🇿 <span>Czech Republic</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇫🇷 <span>France</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇩🇪 <span>Germany</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇮🇳 <span>India</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇮🇱 <span>Israel</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇮🇹 <span>Italy</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇱🇻 <span>Latvia</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇳🇱 <span>Netherlands</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇵🇱 <span>Poland</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇸🇬 <span>Singapore</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇪🇸 <span>Spain</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇸🇪 <span>Sweden</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇺🇦 <span>Ukraine</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇬🇧 <span>United Kingdom</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇺🇸 <span>United States</span></li></label>
                                 <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇻🇺 <span>Vanuatu</span></li></label>
                             </ul>
                        </ul>

                        <ul className={"country--from--header"}>
                            <li className={"country--from--header-title"} value={getCountryTo} onClick={showCountryTo}>Ship To
                                <FontAwesomeIcon className={"chevronVisible" + ((getCountryTo? " chevronInvisible" : ""))} icon={faChevronDown} />
                                <FontAwesomeIcon className={"chevronVisible" + ((getCountryTo? "" : " chevronInvisible"))} icon={faChevronUp}/>
                            </li>
                            <ul className={"country--from--list" + ((getCountryTo)? " country--from--list--visible": "")} >
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇦🇺 <span>Australia</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇨🇳 <span>China</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇨🇿 <span>Czech Republic</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇫🇷 <span>France</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇩🇪 <span>Germany</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇮🇳 <span>India</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇮🇱 <span>Israel</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇮🇹 <span>Italy</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇱🇻 <span>Latvia</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇳🇱 <span>Netherlands</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇵🇱 <span>Poland</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇸🇬 <span>Singapore</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇪🇸 <span>Spain</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇸🇪 <span>Sweden</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇺🇦 <span>Ukraine</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇬🇧 <span>United Kingdom</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇺🇸 <span>United States</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/> 🇻🇺 <span>Vanuatu</span></li></label>
                            </ul>
                        </ul>

                    {/*    */}
                        <ul  className={"country--from--header"} >
                            <li value={getSupplier} onClick={showSupplier} className={"country--from--header-title"}>Select Supplier
                                <FontAwesomeIcon className={"chevronVisible" + ((getSupplier? " chevronInvisible" : ""))} icon={faChevronDown} />
                                <FontAwesomeIcon className={"chevronVisible" + ((getSupplier? "" : " chevronInvisible"))} icon={faChevronUp}/>
                            </li>
                            <ul className={"country--from--list" + ((getSupplier)? " country--from--list--visible": "")} >
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> US-Supplier103</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> IL-Supplier104</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> UK-Supplier105</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> GE-Supplier106</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> UK-Supplier108</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> IT-Supplier110</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> IT-Supplier110</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> US-Supplier113</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> GE-Supplier114</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> SP-Supplier115</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> US-Supplier116</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> IT-Supplier117</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> SP-Supplier118</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> IT-Supplier119</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> SG-Supplier121</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> LT-Supplier123</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> US-Supplier124</span></li></label>
                                <label><li className={"country--from--list-item"}><input type={"checkbox"}/><span> UK-Supplier125</span></li></label>
                            </ul>
                        </ul>

                    </div>
                    <div className={"range"}>
                        <div className={"range--price"}>
                            <p>Price range</p>
                            <Slider slideMin={slideMin} setSlideMin={setSlideMin} slideMax={slideMax} setSlideMax={setSlideMax} min={min} max={max} />
                            <div className={"range--input"}>
                                <span className={"min--price"}>
                                    <p>$</p>
                                    <span>{slideMin}</span>
                                </span>
                                <span className={"max--price"}>
                                    <p>$</p>
                                    <span>{slideMax}</span>
                                </span>
                            </div>
                        </div>
                        <div className={"range--profit"}>
                            <p>Profit range</p>
                            <Slider min={2} max={98} />
                            <div className={"range--input"}>
                                <span className={"min--profit"}>
                                    <p>%</p>
                                </span>
                                <span className={"max--profit"}>
                                    <p>%</p>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className={"reset--BTN"}>
                        <Button title={"Reset Filter"} ultraBig/>
                    </div>
                </div>
            </aside>
        </div>


    )
}

export default Aside;