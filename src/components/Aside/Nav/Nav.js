import "./Nav.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faTachometerAlt} from '@fortawesome/free-solid-svg-icons'
import {faListUl} from '@fortawesome/free-solid-svg-icons'
import {faDiceD6} from '@fortawesome/free-solid-svg-icons'
import {faShoppingCart} from '@fortawesome/free-solid-svg-icons'
import {faClipboardCheck} from '@fortawesome/free-solid-svg-icons'
import {faExchangeAlt} from '@fortawesome/free-solid-svg-icons'
import {faClipboardList} from '@fortawesome/free-solid-svg-icons'
import Logo from "./Logo.js"
const Nav = () => {
    return (
        <div className={"Nav_wrapper"}>
            <nav>
                <div className={"logo"}>
                    <Logo/>
                </div>
                <ul className={"nav__list"}>
                    <li className={"nav__list-item"}> <div className={"img--frame"}><img src={"https://app.365dropship.com/assets/images/profile-example.jpg"}/></div> </li>
                    <li className={"nav__list-item"}> <FontAwesomeIcon icon={faTachometerAlt} /></li>
                    <li className={"nav__list-item active"}> <FontAwesomeIcon icon={faListUl} /></li>
                    <li className={"nav__list-item"}> <FontAwesomeIcon icon={faDiceD6} /></li>
                    <li className={"nav__list-item"}> <FontAwesomeIcon icon={faShoppingCart} /></li>
                    <li className={"nav__list-item"}> <FontAwesomeIcon icon={faClipboardCheck} /></li>
                    <li className={"nav__list-item"}> <FontAwesomeIcon icon={faExchangeAlt} /></li>
                    <li className={"nav__list-item"}> <FontAwesomeIcon icon={faClipboardList} /></li>
                </ul>
            </nav>
        </div>

    )
}

export default Nav;