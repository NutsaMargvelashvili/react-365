import { useState } from "react";
import { useRef } from "react";


const Slider = ({ min, max, setSlideMin, slideMin, setSlideMax, slideMax}) => {

    const minValRef = useRef(min);
    const maxValRef = useRef(max);
    const [minVal, setMinVal] = useState(min);
    const [maxVal, setMaxVal] = useState(max);

        console.log(slideMin)

    return (
        <>
            <input
                type="range"
                min={min}
                max={max}
                // value={maxVal}
                value={slideMin}
                onChange={event => {
                    const value = Math.min(Number(event.target.value), maxVal - 1);
                    setMinVal(value);
                    // console.log(event.target.value);
                    minValRef.current = value;
                    setSlideMin(event.target.value);
                }}
                className="thumb thumb--left"
                style={{ zIndex: minVal > max - 100 && "5" }}
            />
            <input
                type="range"
                min={min}
                max={max}
                // value={maxVal}
                value={slideMax}
                onChange={event => {
                    const value = Math.max(Number(event.target.value), minVal + 1);
                    setMaxVal(value);
                    maxValRef.current = value;
                    setSlideMax(event.target.value);
                }}
                className="thumb thumb--right"
            />
            <div className="slider">
                <div className="slider__track" />
                <div className="slider__range" />
            </div>
        </>
    );
};


export default Slider