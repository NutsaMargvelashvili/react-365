
const Button = (props) => {
    const selectAll = () => {
        props.setAllSelected(!props.allSelected);
        // console.log(props.allSelected);
    }
    const deselectAll = () => {
        props.setClearClicked(!props.clearClicked);
    }
    return(
            <button onClick={(props.title == "select all")? selectAll: (props.title == "clear")? deselectAll:""} className={"button" + ((props.big ? " button--big" : props.ultraBig ? " button--Ultra-Big" : props.extraBig ? " button--extra-Big" : ""))} id={((props.clearBTN)?"clearBTN":"") + ((props.allDeselected)? "--visible":"--invisible")}>{props.title}</button>
        );

}
export default Button;