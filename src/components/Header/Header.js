import "./Header.css"
import Button from "./Button.js"
import Sort from "./Sort.js"
import Search from "./Search.js"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faQuestion} from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import React from "react";

const Header = ({ setSearchInput , setBTNVal, setSort, getSort, product,  selected, setAllSelected, allSelected, setAllDeselected, allDeselected, clearClicked, setClearClicked}) => {

    return (
        <div className={"header_wrapper"}>
            <div className="Header">
                <div className={"aside__filter-small"}>
                    <Button title={"filter"}/>
                </div>
                <div className={"selected__products-number"}>
                    <Button setAllSelected={setAllSelected}  allSelected={allSelected} title={"select all"} />
                    <p>selected {selected} out of {(product && product[0])?product.length:"0"} products</p>
                    <Button title={"clear"} setAllDeselected={setAllDeselected} allDeselected={allDeselected}  clearClicked={clearClicked} setClearClicked={setClearClicked} clearBTN />
                </div>
                <div className={"selected__products-number-small"}>
                    <button className={"button--small"}><FontAwesomeIcon icon={faCheckCircle} /></button>
                </div>
                <div  className={"nav-search"}>
                    <Search setSearchInput={setSearchInput} setBTNVal={setBTNVal}/>
                    <Button title={"add to inventory"} big/>
                    <div className={"menu"}>
                        <button className={"menu__BTN"}>  <FontAwesomeIcon icon={faBars} /></button>
                    </div>
                    <div className={"help"}>
                        <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
                    </div>

                </div>
            </div>
            <div className={"Header__sort-filter"}>
                <Sort setSort={setSort} getSort={getSort} />
            </div>
        </div>

    )
}

export default Header;
