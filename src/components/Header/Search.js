import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const Search = ({ setBTNVal, setSearchInput}) => {

    const searchInput = (event) => {
        setSearchInput(event.target.value);
    }
    

    return(
        <div className={'Header__search'}>
            <div className="Search">
                <input type={'text'} placeholder={'search...'} className={"Search__products"} onChange={searchInput} />
                <button className={'Search__ICON'} onClick={setBTNVal}>
                    <FontAwesomeIcon icon={faSearch} />
                </button>
            </div>
        </div>

    );

}
export default Search;
