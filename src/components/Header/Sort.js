import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faAlignJustify} from '@fortawesome/free-solid-svg-icons'
import {faSort} from '@fortawesome/free-solid-svg-icons'
import React, { useState } from 'react';
const Sort = ({setSort, getSort}) => {
    const [getVal, setVal] = useState(false);
    const [getSortVal, setSortVal] = useState("New Arrivals");
    const dropdownDisplay = () => {
        setVal(!getVal);
        console.log(getVal);
    }
    const sortable = (event) => {

        setSortVal(event.target.innerHTML);
        if(event.target.innerHTML == "From A to Z"){
            setSort("AtoZ")
        }
        else if(event.target.innerHTML == "From Z to A"){
            setSort("ZtoA")
        }
        else if(event.target.innerHTML == "Price: High to Low"){
            setSort("priceDesc")
        }
        else if(event.target.innerHTML == "Price: Low to High"){
            setSort("priceAsc")
        }
        else if(event.target.innerHTML == "New Arrivals"){
            setSort("New")
        }
        console.log(getSortVal);
    }
    return(
        <div className={"nav_sort"}>


            <ul className={"sort__list"}>
                <li className="sort" value={getVal} onClick={dropdownDisplay}>

                   <p><FontAwesomeIcon  className={"FontAwesomeIcon" + ((getVal)? "--darker": "")} icon={faAlignJustify}/></p>
                    <p className={"sortBy"}>Sort By: </p><p className={"sorted"}>{(getSortVal)}</p>
                    <ul className={"dropdown" + ((getVal)? " visible": "")}>
                        <li className={"" + ((getSortVal === "New Arrivals")? "active": "")}><p onClick={sortable}>New Arrivals</p></li>
                        <li className={"" + ((getSortVal === "Price: Low to High")? "active": "")}><p onClick={sortable}>Price: Low to High</p></li>
                        <li className={"" + ((getSortVal === "Price: High to Low")? "active": "")}><p  onClick={sortable}>Price: High to Low</p></li>
                        <li className={"" + ((getSortVal === "From A to Z")? "active": "")}><p onClick={sortable}>From A to Z</p></li>
                        <li className={"" + ((getSortVal === "From Z to A")? "active": "")}><p onClick={sortable}>From Z to A</p></li>
                    </ul>
                  <p> <FontAwesomeIcon className={"FontAwesomeIcon" + ((getVal)? "--darker": "")} icon={faSort}/></p>

                </li>
            </ul>

        </div>

    );

}
export default Sort;