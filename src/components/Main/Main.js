import "./Main.css"
import Product from "./Product";
import React, {useEffect, useState} from 'react';
import {API} from "../../Config";
import Header from "../Header/Header";
import Button from "../Header/Button";
import {faCheckCircle, faTimes} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Main = ({isLoading, clicked, setClicked, productClicked, setProductClicked, product, setSelected, selected,allSelected, allDeselected, setAllDeselected, clearClicked, setClearClicked}) => {
    console.log(isLoading , 'isLoading')

    // const unzoomProduct = () => {
    //     // setProductClicked(products);
    //     setClicked(!clicked);
    //     // console.log(productClicked);
    // }
    const unzoomProduct = () => {
        // setProductClicked(products);
        setClicked(!clicked);
        // console.log(productClicked);
    }
    const zoomedProduct = () => {
        // setProductClicked(products);
        setClicked(true);
        // console.log(productClicked);
    }
    return (
    <>
        {isLoading ? <div className={"main_wrapper"}>
            <div className="loadingGif">
              <p>loading...</p>
            </div></div> : (
            <div className={"main_wrapper"}>
                {product && product[0] && product.map((products) => {return <Product clicked={clicked} setClicked={setClicked} selected={selected} setSelected={setSelected} allSelected={allSelected} product={product} setAllDeselected={setAllDeselected} allDeselected={allDeselected} clearClicked={clearClicked} setClearClicked={setClearClicked} productClicked={productClicked} setProductClicked={setProductClicked} products={products} image={products.image} title={products.title} price={products.price}  />})}
                <div  className={"transparent" + ((clicked)? "-clicked": "")}>
                    <div className={"product--opened" + ((clicked)? "-clicked": "")}>
                        <div className={"item-clicked"}>
                            <div className={"item--info"}>
                                <div className={"item--price"}>
                                    <div className="catalog__price-zoomed">
                                        <div className="RRP">
                                            <p>$10</p>
                                            <p>RRP</p>
                                        </div>
                                        <div className="Profit">
                                            {productClicked && (<p className={"description"}>${productClicked.price}</p>)}
                                            <p>cost
                                            </p>
                                            <div className={"separator"}></div>
                                        </div>
                                        <div className="Cost">
                                            <p>60% ($6)</p>
                                            <p>profit</p>
                                            <div className={"separator"}></div>
                                        </div>
                                    </div></div>
                                <div className={"item--image"}>
                                    <div className={"zoom-wrapper"}>
                                        {productClicked && (<img className={"zoomImage"} src={productClicked.image}/>)}
                                    </div>
                                </div>
                            </div>
                            <div className={"item--description"}>
                                <div  className={"closeModal"}><button onClick={unzoomProduct}><FontAwesomeIcon icon={faTimes} /></button></div>
                                <p className={"copy"}><span>SKU# bgb-s2412499 COPY</span>
                                    <span  className={"item--supplier"}>supplier: <a href={""}>US-Supplier146</a></span>
                                </p>
                                {productClicked && (<p className={"title"}>{productClicked.title}</p>)}
                                <div className={"addToInventory"}><Button title={"add to my inventory"} extraBig/></div>
                                <div className={"details"}>
                                    <p>
                                        <span id={"activeOpt"}>Product Details</span>
                                        <span>shipping rates</span>
                                    </p>
                                </div>
                                <div className={"description__wrapper"}>
                                    {productClicked && (<p className={"desc"}>{productClicked.description}   </p>)}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )}
    </>

    )
}

export default Main;