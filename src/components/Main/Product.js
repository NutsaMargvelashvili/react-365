import Button from "../Header/Button";
import React, {useEffect, useState} from 'react';
import {API} from "../../Config";
import Header from "../Header/Header";

const Product = ({clicked, setClicked, productClicked, setProductClicked, products, setSelected, selected, image, title, price, allSelected, product, clearClicked, setClearClicked, setAllDeselected}) => {

    const [getInput, setInput] = useState(false);


    const showValue = (event) => {
        console.log(event.target.checked);
        setInput(event.target.checked);
        if(event.target.checked){

            setSelected(selected + 1);
        }
        else{
            setSelected(selected - 1);
        }
     console.log(selected);

    }
    const zoomProduct = () => {
        setProductClicked(products);
        setClicked(!clicked);

        // console.log(productClicked);
    }
    // const zoomProduct = () => {
    //     setProductClicked(products);
    //     setClicked(!clicked);
    //     // console.log(productClicked);
    // }
    useEffect( () => {
        if(allSelected){
            setInput(true);
            setSelected((product && product[0])?product.length:"0");
        }
    }, [allSelected]);
    useEffect( () => {
        if(clearClicked){
            setInput(false);
            setSelected(0);
        }
    }, [clearClicked]);
    useEffect( () => {
        setClicked(false);
      if(selected> 0){
          setAllDeselected(true);
      }
      else{
          setAllDeselected(false);
      }
    }, [getInput]);

    return(
        // <div onClick={unzoomProduct} className={"transparent" + ((clicked)? "-clicked": "")}>
        // <div className={"product--opened" + ((clicked)? "-clicked": "")}>
        //     <div className={"item" + ((clicked)? "-clicked": "")}>
        //         <div className={"item--info"}>
        //             <div className={"item--price"}>
        //                 <div className="catalog__price-zoomed">
        //                 <div className="RRP">
        //                     <p>$10</p>
        //                     <p>RRP</p>
        //                 </div>
        //                 <div className="Profit">
        //                     {productClicked && (<p className={"description"}>${productClicked.price}</p>)}
        //                     <p>cost
        //                     </p>
        //                     <div className={"separator"}></div>
        //                 </div>
        //                 <div className="Cost">
        //                     <p>60% ($6)</p>
        //                     <p>profit</p>
        //                     <div className={"separator"}></div>
        //                 </div>
        //             </div></div>
        //             <div className={"item--image"}>
        //                 <div className={"zoom-wrapper"}>
        //                     {productClicked && (<img className={"zoomImage"} src={productClicked.image}/>)}
        //                 </div>
        //             </div>
        //         </div>
        //         <div className={"item--description"}>
        //             <p className={"copy"}><span>SKU# bgb-s2412499 COPY</span>
        //                 <span  className={"item--supplier"}>supplier: <a href={""}>US-Supplier146</a></span>
        //             </p>
        //             {productClicked && (<p className={"title"}>{productClicked.title}</p>)}
        //             <div className={"addToInventory"}><Button title={"add to my inventory"} extraBig/></div>
        //             <div className={"details"}>
        //                 <p>
        //                     <span id={"activeOpt"}>Product Details</span>
        //                     <span>shipping rates</span>
        //                 </p>
        //             </div>
        //             <div className={"description__wrapper"}>
        //                 {productClicked && (<p className={"desc"}>{productClicked.description}   </p>)}
        //             </div>
        //
        //         </div>
        //         </div>

            <div onClick={zoomProduct} className={"product__item" + ((getInput)? " product__item--highlighted": "") + ((clicked)? "-clicked": "")}>


                    <div className={"product--head" + ((getInput)? " product--head--visible": "")}>
                        <div className={"checkbox-cont"}>
                            <input type="checkbox" checked={getInput} onChange={showValue}/>
                            </div>
                        <div className={"btn-area" + ((getInput)? " product--head--invisible": "")}>
                            <div>
                                <Button title={"Add To Inventory"}/>
                            </div>

                        </div>
                    </div>


                    <div  className="catalog__product">
                        <div className="catalog__photo">
                            <img src={image}/>
                        </div>
                        <div className="catalog__title">
                            <p>{title}</p>

                        </div>
                        <div className={"supplier"}>
                            <p>By: <a href={""}>US-Supplier146</a></p>
                        </div>
                        <div className="catalog__price">
                            <div className="RRP">
                                <p>$10</p>
                                <p>RRP</p>
                            </div>
                            <div className="Profit">
                                <p>${price}</p>
                                <p>cost
                                </p>
                                <div className={"separator"}></div>
                            </div>
                            <div className="Cost">
                                <p>60% ($6)</p>
                                <p>profit</p>
                                <div className={"separator"}></div>
                            </div>
                        </div>
                    </div>


            </div>
        // </div>
        // </div>
    );

}
export default Product;